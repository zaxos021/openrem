########################
Upgrade to OpenREM 0.8.0
########################

****************
Headline changes
****************

* Charts: Added mammography scatter plot, thanks to `@rijkhorst`_

***************************************************
Upgrading an OpenREM server with no internet access
***************************************************


****************************
Upgrading from version 0.7.3
****************************



..  _@rijkhorst: https://bitbucket.org/rijkhorst/