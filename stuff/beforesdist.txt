Change the version number in 
	openrem/remapp/version.py
	install instructions in the release notes where applicable
	    install.rst - set to version for beta or versionless for release
	    release-0.x.x.rst - set to version (upgrades always need version specified)
	    install-offline.rst (twice)
	    upgrade-offline.rst (twice)
	date and other details in changes and CHANGES
    Edit README.rst
    Edit description in setup.py between beta and release

Clean the existing distribute folder:
	rm -r *

Then get the new contents:
rsync -av --exclude-from='../bbOpenREM/stuff/distexclude.txt' ../bbOpenREM/ .
cp ../bbOpenREM/stuff/0002_0_7_fresh_install_add_median.py.inactive openrem/remapp/migrations/

Build:
	python setup.py sdist bdist_wheel

